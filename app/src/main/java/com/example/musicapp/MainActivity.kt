package com.example.musicapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.musicapp.data.MyPref
import com.example.musicapp.utils.MyAppManager
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }


    override fun onPause() {
        super.onPause()
        MyPref.lastMusicPosition(MyAppManager.selectMusicPos)
        MyPref.lastMusicDuration(MyAppManager.currentTime)
    }
}