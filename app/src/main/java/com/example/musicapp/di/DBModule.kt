package com.example.musicapp.di

import android.content.Context
import androidx.room.Room
import com.example.musicapp.data.local.AppDB
import com.example.remindapp.data.local.dao.MusicDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DBModule {
    @[Provides Singleton]
    fun provideAppDatabase(@ApplicationContext context: Context):AppDB =
        Room.databaseBuilder(context, AppDB::class.java,"Reminder.db")
            .allowMainThreadQueries()
            .build()

    @[Provides Singleton]
    fun provideContactDao(dataBase:AppDB) : MusicDao = dataBase.getDao()

}