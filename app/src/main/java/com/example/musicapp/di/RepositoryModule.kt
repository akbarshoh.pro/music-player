package com.example.musicapp.di

import com.example.musicapp.damain.AppRepository
import com.example.remindapp.domain.AppRepositoryImp
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {
    @Binds
    fun bindRepo(imp: AppRepositoryImp) : AppRepository
}