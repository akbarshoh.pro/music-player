package com.example.musicapp.presentation.screen.playmuz

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.musicapp.R
import com.example.musicapp.damain.AppRepository
import com.example.musicapp.data.ActionEnum
import com.example.musicapp.data.MusicData
import com.example.musicapp.data.MyPref
import com.example.musicapp.data.local.entity.MusicEntity
import com.example.musicapp.databinding.ScreenPlayerBinding
import com.example.musicapp.service.MyService
import com.example.musicapp.utils.MyAppManager
import com.example.musicapp.utils.getItemByPosition
import com.example.musicapp.utils.setChangeProgress
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PlayMuzScreen : Fragment() {
    private var _binding: ScreenPlayerBinding? = null
    private val binding get() = _binding!!
    private var time = System.currentTimeMillis()

    @Inject
    lateinit var repo: AppRepository

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenPlayerBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("LogNotTimber")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        MyAppManager.isPlayingLiveData.observe(viewLifecycleOwner, isManageObserver)
        MyAppManager.playMusicLiveData.observe(viewLifecycleOwner, playMusicLiveData)
        MyAppManager.currentTimeLiveData.observe(viewLifecycleOwner, currentTimeObserver)

        if (repo.getById(MyAppManager.cursor!!.getItemByPosition(MyAppManager.selectMusicPos).id).isEmpty()) {
            binding.like.visibility = View.INVISIBLE
            binding.likeNo.visibility = View.VISIBLE
        } else {
            binding.like.visibility = View.VISIBLE
            binding.likeNo.visibility = View.INVISIBLE
        }


        binding.like.setOnClickListener {
            val data = MyAppManager.cursor!!.getItemByPosition(MyAppManager.selectMusicPos)
            repo.deleteMusic(
                MusicEntity(
                    id = data.id,
                    artist = data.artist,
                    title = data.title,
                    data = data.data,
                    duration = data.duration,
                    pos = MyAppManager.selectMusicPos
                )
            )
            binding.like.visibility = View.INVISIBLE
            binding.likeNo.visibility = View.VISIBLE
        }

        binding.likeNo.setOnClickListener {
            val data = MyAppManager.cursor!!.getItemByPosition(MyAppManager.selectMusicPos)
            repo.addMusic(
                MusicEntity(
                    id = data.id,
                    artist = data.artist,
                    title = data.title,
                    data = data.data,
                    duration = data.duration,
                    pos = MyAppManager.selectMusicPos
                )
            )

            binding.like.visibility = View.VISIBLE
            binding.likeNo.visibility = View.INVISIBLE
        }

        binding.buttonNext.setOnClickListener {
            startServes(ActionEnum.NEXT)
            binding.seekBar.progress = 0
        }
        binding.buttonPrev.setOnClickListener {
            startServes(ActionEnum.PREV)
            binding.seekBar.progress = 0
        }
        binding.buttonManage.setOnClickListener {
            startServes(ActionEnum.MANAGE)
        }

        binding.playlist.setOnClickListener {
            if (System.currentTimeMillis() - time > 1000) {
                findNavController().popBackStack()
            }
        }

        binding.blast.playAnimation()


        if (MyAppManager.selectMusicPos != -1) {
            val data = MyAppManager.cursor!!.getItemByPosition(MyAppManager.selectMusicPos)
            binding.textMusicName.text = data.title
            binding.textArtistName.text = data.artist
            binding.seekBar.progress = MyAppManager.currentTime.toInt()
        }



        binding.seekBar.setChangeProgress { progress, fromUser ->
            if (fromUser) {
                MyAppManager.currentTime = progress.toLong()
                startServes(ActionEnum.SEEK)
                binding.currentTime.text = getTime(progress.toLong())
            }
        }

        if (MyPref.replay()) {
            binding.btnRepYes.visibility = View.VISIBLE
            binding.btnRepNo.visibility = View.INVISIBLE
        } else {
            binding.btnRepYes.visibility = View.INVISIBLE
            binding.btnRepNo.visibility = View.VISIBLE
        }

        binding.btnRepYes.setOnClickListener {
            binding.btnRepYes.visibility = View.INVISIBLE
            binding.btnRepNo.visibility = View.VISIBLE
            MyPref.replay(false)
        }

        binding.btnRepNo.setOnClickListener {
            binding.btnRepYes.visibility = View.VISIBLE
            binding.btnRepNo.visibility = View.INVISIBLE
            MyPref.replay(true)
        }

        binding.back.setOnClickListener {
            if (System.currentTimeMillis() - time > 1000) {
                findNavController().popBackStack()
            }
        }

    }


    private val isManageObserver = Observer<Boolean> {
        if (it){
            binding.buttonManage.setImageResource(R.drawable.ic_pause)
            binding.blast.playAnimation()
        }
        else{
            binding.buttonManage.setImageResource(R.drawable.ic_play)
            binding.blast.pauseAnimation()
        }

    }

    @SuppressLint("LogNotTimber", "SetTextI18n")
    private val playMusicLiveData = Observer<MusicData> {
        binding.textMusicName.text = it.title
        binding.textArtistName.text = it.artist
        binding.seekBar.max = MyAppManager.fullTime.toInt()

        binding.totalTime.text = getTime(MyAppManager.fullTime)
        binding.currentTime.text = getTime(MyAppManager.currentTime)

    }

    private fun startServes(enum: ActionEnum) {
        val intent = Intent(requireContext(), MyService::class.java)
        intent.putExtra("COMMAND", enum)
        if (Build.VERSION.SDK_INT > 29) {
            requireActivity().startForegroundService(intent)
        } else requireActivity().startService(intent)
    }

    private val currentTimeObserver = Observer<Long> {
        binding.seekBar.progress = it.toInt()
        binding.currentTime.text = getTime(it)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun getTime(dur: Long) : String {
        if (dur >= 3600000) {
            val hours: Long = dur / 1000 / 60 / 60
            val minutes: Long = (dur / 1000 / 60) % 60
            val seconds: Long = (dur / 1000) % 60
            return String.format("%02d:%02d:%02d", hours, minutes, seconds)
        }

        val seconds: Long = dur / 1000 % 60
        val minutes: Long = dur / 1000 / 60
        return String.format("%02d:%02d", minutes, seconds)
    }
}