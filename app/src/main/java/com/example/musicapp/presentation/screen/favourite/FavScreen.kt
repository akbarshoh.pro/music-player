package com.example.musicapp.presentation.screen.favourite

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.musicapp.damain.AppRepository
import com.example.musicapp.data.ActionEnum
import com.example.musicapp.databinding.ScreenLikeBinding
import com.example.musicapp.presentation.adapter.MainAdapter
import com.example.musicapp.service.MyService
import com.example.musicapp.utils.MyAppManager
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FavScreen : Fragment() {
    private var _binding: ScreenLikeBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var repo: AppRepository

    private val adapter: MainAdapter by lazy { MainAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenLikeBinding.inflate(layoutInflater)
        return binding.root
    }


    @SuppressLint("LogNotTimber")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvMusics.adapter = adapter
        binding.rvMusics.layoutManager = LinearLayoutManager(requireContext())

        adapter.selectedMusic {
            MyAppManager.selectMusicPos = it
            MyAppManager.currentTime = 0
            startMyService(ActionEnum.PLAY)
            findNavController().navigate(FavScreenDirections.actionFavScreenToPlayMuzScreen())
        }

        if (repo.getAll().isEmpty()) {
            binding.empty.visibility = View.VISIBLE
        } else {
            binding.empty.visibility = View.GONE
            adapter.submitList(repo.getAll())
        }

        binding.back.setOnClickListener {
            findNavController().popBackStack()
        }

    }

    private fun startMyService(action  : ActionEnum) {
        val intent = Intent(requireContext(), MyService::class.java)
        intent.putExtra("COMMAND", action)
        if (Build.VERSION.SDK_INT >= 26) {
            requireActivity().startForegroundService(intent)
        } else requireActivity().startService(intent)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}