package com.example.musicapp.presentation.screen.player

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.musicapp.R
import com.example.musicapp.data.ActionEnum
import com.example.musicapp.data.MusicData
import com.example.musicapp.data.MyPref
import com.example.musicapp.databinding.ScreenListBinding
import com.example.musicapp.presentation.adapter.MusicsAdapter
import com.example.musicapp.service.MyService
import com.example.musicapp.utils.MyAppManager
import com.example.musicapp.utils.getItemByPosition
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PlayerScreen : Fragment(R.layout.screen_list) {
    private val adapter: MusicsAdapter by lazy { MusicsAdapter() }
    private var _binding: ScreenListBinding? = null
    private val binding get() = _binding!!
    private var time = System.currentTimeMillis()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ScreenListBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvMusics.adapter = adapter
        binding.rvMusics.layoutManager = LinearLayoutManager(requireContext())

        if (MyAppManager.cursor != null && MyAppManager.cursor!!.moveToFirst()) {
            adapter.cursor = MyAppManager.cursor
            binding.empty.visibility = View.GONE
        } else {
            binding.empty.visibility = View.VISIBLE
        }

        if (MyPref.lastMusicPosition() != -1) {
            binding.frameLayout.visibility = View.VISIBLE
            MyAppManager.selectMusicPos = MyPref.lastMusicPosition()
            MyAppManager.currentTime = MyPref.lastMusicDuration()
            MyAppManager.fullTime = MyAppManager.cursor!!.getItemByPosition(MyPref.lastMusicPosition()).duration

            binding.name.text = MyAppManager.cursor!!.getItemByPosition(MyAppManager.selectMusicPos).title
        }

        binding.manage.setOnClickListener {
            startMyService(ActionEnum.MANAGE)
        }
        binding.next.setOnClickListener {
            startMyService(ActionEnum.NEXT)
        }
        binding.prev.setOnClickListener {
            startMyService(ActionEnum.PREV)
        }

        adapter.selectedMusic {
            MyAppManager.selectMusicPos = it
            MyAppManager.currentTime = 0
            startMyService(ActionEnum.PLAY)
            binding.frameLayout.visibility = View.VISIBLE
        }

        binding.frameLayout.setOnClickListener {
            if (System.currentTimeMillis() - time > 1000) {
                findNavController().navigate(PlayerScreenDirections.actionPlayerScreenToPlayMuzScreen())
                time = System.currentTimeMillis()
            }
        }

        binding.toLikeScreen.setOnClickListener {
            if (System.currentTimeMillis() - time > 1000) {
                findNavController().navigate(PlayerScreenDirections.actionPlayerScreenToFavScreen())
                time = System.currentTimeMillis()
            }
        }

        MyAppManager.playMusicLiveData.observe(viewLifecycleOwner, playMusicObserver)
        MyAppManager.isPlayingLiveData.observe(viewLifecycleOwner, isPlayingObserver)

    }

    private fun startMyService(action  : ActionEnum) {
        val intent = Intent(requireContext(), MyService::class.java)
        intent.putExtra("COMMAND", action)
        if (Build.VERSION.SDK_INT >= 26) {
            requireActivity().startForegroundService(intent)
        } else requireActivity().startService(intent)
    }

    private val playMusicObserver = Observer<MusicData> { data ->
        binding.apply {
            name.text= data.title
        }
    }

    private val isPlayingObserver = Observer<Boolean> {
        if (it)
            binding.manage.setImageResource(R.drawable.ic_pause)
        else
            binding.manage.setImageResource(R.drawable.ic_play)

        binding.frameLayout.visibility = View.VISIBLE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}