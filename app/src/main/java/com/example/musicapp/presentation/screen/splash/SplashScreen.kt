package com.example.musicapp.presentation.screen.splash

import android.Manifest
import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.musicapp.R
import com.example.musicapp.utils.MyAppManager
import com.example.musicapp.utils.checkPermissions
import com.example.musicapp.utils.getMusicsCursor
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@SuppressLint("CustomSplashScreen")
@AndroidEntryPoint
class SplashScreen : Fragment(R.layout.screen_splash) {
    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().checkPermissions(arrayOf(Manifest.permission.READ_MEDIA_AUDIO,Manifest.permission.POST_NOTIFICATIONS)) {
            requireContext().getMusicsCursor()
                .onEach {
                    MyAppManager.cursor = it
                    lifecycleScope.launch {
                        delay(1000)
                        findNavController().navigate(SplashScreenDirections.actionSplashScreenToPlayerScreen())
                    }
                }
                .launchIn(lifecycleScope)
        }
    }
}