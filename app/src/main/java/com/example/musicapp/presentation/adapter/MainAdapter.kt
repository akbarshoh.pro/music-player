package com.example.musicapp.presentation.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.musicapp.data.local.entity.MusicEntity
import com.example.musicapp.databinding.ItemMusicBinding
import com.example.musicapp.utils.MyAppManager
import com.example.musicapp.utils.getItemByPosition

class MainAdapter : ListAdapter<MusicEntity, MainAdapter.VH>(Diff) {
    private var selectedMusicItem: ((Int) -> Unit)? = null

    inner class VH(private val item: ItemMusicBinding) : RecyclerView.ViewHolder(item.root) {
        init {
            item.root.setOnClickListener {
                selectedMusicItem?.invoke(getItem(adapterPosition).pos)
            }
        }
        @SuppressLint("SetTextI18n")
        fun bind(data: MusicEntity) {
            item.textMusicName.text = data.title
            item.textArtistName.text = data.artist
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH =
        VH(ItemMusicBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: VH, position: Int) =
        holder.bind(getItem(position))

    object Diff : DiffUtil.ItemCallback<MusicEntity>() {
        override fun areItemsTheSame(oldItem: MusicEntity, newItem: MusicEntity): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: MusicEntity, newItem: MusicEntity): Boolean =
            oldItem == newItem

    }

    fun selectedMusic(block: (Int) -> Unit) {
        selectedMusicItem = block
    }

}