package com.example.musicapp.presentation.adapter

import android.annotation.SuppressLint
import android.database.Cursor
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.musicapp.data.MyPref
import com.example.musicapp.databinding.ItemMusicBinding
import com.example.musicapp.utils.MyAppManager
import com.example.musicapp.utils.getItemByPosition

class MusicsAdapter : RecyclerView.Adapter<MusicsAdapter.VH>() {
    var cursor: Cursor? = null
    private var selectedMusicItem: ((Int) -> Unit)? = null

    @SuppressLint("NotifyDataSetChanged")
    inner class VH(private val item: ItemMusicBinding) : RecyclerView.ViewHolder(item.root) {
        init {
            item.root.setOnClickListener {
                selectedMusicItem?.invoke(adapterPosition)
            }
        }
        fun bind(position: Int) {
            val data = cursor?.getItemByPosition(position)!!
            item.textMusicName.text = data.title
            item.textArtistName.text = data.artist
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH =
        VH(item = ItemMusicBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun getItemCount(): Int = cursor?.count ?: 0

    override fun onBindViewHolder(holder: VH, position: Int) = holder.bind(position)

    fun selectedMusic(block: (Int) -> Unit) {
        selectedMusicItem = block
    }

}