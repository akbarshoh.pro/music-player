package com.example.musicapp.app

import android.app.Application
import com.example.musicapp.data.MyPref
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        MyPref.init(this)
    }
}