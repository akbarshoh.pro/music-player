package com.example.musicapp.utils

import android.widget.SeekBar

fun SeekBar.setChangeProgress(block : (Int, Boolean) -> Unit) {
    this.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
            p0?.let { block.invoke(it.progress, p2) }
        }

        override fun onStartTrackingTouch(p0: SeekBar?) {}

        override fun onStopTrackingTouch(p0: SeekBar?) {}
    })
}

