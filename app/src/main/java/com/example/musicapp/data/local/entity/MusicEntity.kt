package com.example.musicapp.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MusicEntity(
    @PrimaryKey val id: Int,
    val artist: String?,
    val title: String?,
    val data: String?,
    val duration: Long,
    val pos: Int
)