package com.example.musicapp.data

enum class ActionEnum (val pos: Int) {
    MANAGE(0), PLAY(1), PAUSE(2), NEXT(3), PREV(4), CANCEL(5), SEEK(6), REPLAY(7)
}