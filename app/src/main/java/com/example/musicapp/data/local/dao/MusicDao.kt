package com.example.remindapp.data.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.Companion.REPLACE
import androidx.room.Query
import com.example.musicapp.data.local.entity.MusicEntity

@Dao
interface MusicDao {
    @Query("SELECT * FROM musicentity")
    fun getAll() : List<MusicEntity>
    @Query("SELECT * FROM musicentity WHERE id = :id")
    fun getById(id: Int) : List<MusicEntity>
    @Insert
    fun add(music: MusicEntity)

    @Delete
    fun delete(music: MusicEntity)
}