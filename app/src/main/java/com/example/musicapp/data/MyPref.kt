package com.example.musicapp.data

import android.content.Context
import android.content.SharedPreferences

object MyPref {
    private var pref: SharedPreferences? = null

    fun init(context: Context) {
        if (pref == null)
            pref = context.getSharedPreferences("pref", Context.MODE_PRIVATE)
    }

    fun replay() : Boolean =
        pref!!.getBoolean("replay", false)


    fun replay(bool: Boolean) {
        pref!!.edit().putBoolean("replay", bool).apply()
    }

    fun lastMusicPosition() : Int =
        pref!!.getInt("lastMusicPosition", -1)

    fun lastMusicPosition(pos: Int) {
        pref!!.edit().putInt("lastMusicPosition", pos).apply()
    }

    fun lastMusicDuration() : Long =
        pref!!.getLong("lastMusicDuration", 0)

    fun lastMusicDuration(duration: Long) {
        pref!!.edit().putLong("lastMusicDuration", duration).apply()
    }

}