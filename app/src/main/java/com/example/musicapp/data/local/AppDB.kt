package com.example.musicapp.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.musicapp.data.local.entity.MusicEntity
import com.example.remindapp.data.local.dao.MusicDao
import javax.inject.Singleton

@Singleton
@Database(entities = [MusicEntity::class], version = 1)
abstract class AppDB : RoomDatabase() {
    abstract fun getDao() : MusicDao
}