package com.example.musicapp.damain

import com.example.musicapp.data.local.entity.MusicEntity

interface AppRepository {
    fun getAll() : List<MusicEntity>
    fun addMusic(music: MusicEntity)
    fun deleteMusic(music: MusicEntity)
    fun getById(id: Int) : List<MusicEntity>
}