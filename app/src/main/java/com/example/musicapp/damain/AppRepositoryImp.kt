package com.example.remindapp.domain

import com.example.musicapp.damain.AppRepository
import com.example.musicapp.data.local.entity.MusicEntity
import com.example.remindapp.data.local.dao.MusicDao
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppRepositoryImp @Inject constructor(
    private val dao: MusicDao
) : AppRepository {

    override fun getAll(): List<MusicEntity> = dao.getAll()

    override fun addMusic(music: MusicEntity) = dao.add(music)
    override fun deleteMusic(music: MusicEntity) = dao.delete(music)

    override fun getById(id: Int): List<MusicEntity> = dao.getById(id)

}